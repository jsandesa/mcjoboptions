from MadGraphControl.MadGraphUtils import *

# PDF base fragment
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment

nevents = runArgs.maxEvents*2.0 if runArgs.maxEvents>0 else 2.0*evgenConfig.nEventsPerJob

maxjetflavor=5
parton_shower='PYTHIA8'
nJetMax=1
qCut=20.

gridpack_mode=True

if not is_gen_from_gridpack():

    process="""
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ a [QCD] @0
    add process p p > e+ ve a [QCD] @0
    add process p p > e- ve~ a j [QCD] @1
    add process p p > e+ ve a j [QCD] @1
    output -f"""
    process_dir = str(new_process(process))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

#Fetch default NLO run_card.dat and set parameters
settings = {'nevents':int(nevents),
            'ickkw'         : 3,
            'ptj'           : 8.0,
            'etaj'          : 5.5,
            'ptgmin'        : 10,
            'etagamma'      : 3.0,
            'epsgamma'      : 0.1,
            'R0gamma'       : 0.1,
            'xn'            : 2,
            'isoEM'         : True,
            'maxjetflavor'  :int(maxjetflavor),
            'jetradius'     : 1.0,
            'parton_shower' : parton_shower
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

##### Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")                                                                                                                  
include("Pythia8_i/Pythia8_aMcAtNlo.py")  

evgenConfig.description = 'aMcAtNlo_enugamma'
evgenConfig.keywords+=['Wgamma','jets','NLO']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Heather Russell <hrussell@cern.ch>']


# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut

print "Settings to be used for showering:"
print "  PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "  PYTHIA8_qCut = %f"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")
