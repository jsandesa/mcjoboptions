## Config for Py8 tune A14 with LUXqed
## The default version of this tune fragment include EvtGen for standardised b fragmentation

# Reference the non-standard version without EvtGen
get_Pythia8A14LUXqedCommonFile = subprocess.Popen(['get_files', '-jo', 'Pythia8_A14_LUXqed_Common.py'])
if get_Pythia8A14LUXqedCommonFile.wait():
	print "Could not get hold of Pythia8_A14_LUXqed_Common.py, exiting..."
	sys.exit(2)
include("Pythia8_A14_LUXqed_Common.py")

# Add EvtGen for b fragmentation as default.  No EvtGen is available in "nonStandard"
include("Pythia8_i/Pythia8_EvtGen.py")

