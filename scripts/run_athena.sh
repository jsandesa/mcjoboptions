#!/bin/bash

# Make sure that if a command fails the script will continue
set +e

# Original directory
ORIGDIR=$PWD

# Directory for which to run athena
dir=$1
echo "INFO: Will run athena for $dir"

# Extract directory names
DSID=$(basename $dir)
DSIDxxx=$(dirname $dir)

# Move to DSID directory (actually 1 directory up since athena won't run if we go in the same dir...)
cd $DSIDxxx

# There should be only 1 file called log.generate.short in each of these directories
if [ ! -f $DSID/log.generate.short ] ; then
  echo "WARNING: No log.generate.short in $dir"
  echo "athena running will be skipped for this directory, however make sure that the requesters have tested the jO localy!!!"
  cd $ORIGDIR
  exit 0
  # The above solution is not ideal - only temporary until we enforce running athena for all new jO
  # exit 1
else
  echo "OK: log.generate.short found"
fi

# Check if the job is expected to last more than 1 hour
cpu=$(grep CPU $DSID/log.generate.short | awk '{print $8}')
if (( $(echo "$cpu > 1.0" | bc -l) )) ; then
  echo "ERROR: Job is expected to last more than 1h - time estimate: $cpu hours"
  echo "It is not possible to run this job in the CI. Contact the MC software coordinators"
  exit 1
else
  echo "OK: Job expected to last less than 1h - time estimate: $cpu hours"
fi
echo -e "\033[0m" # switch back terminal colour to black

# Setup ATLAS
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

# Get ecmEnergy from the log.generate.short file
ecmEnergy=$(grep 'ecmEnergy =' $DSID/log.generate.short | awk '{print $NF}')
if [ -z $ecmEnergy ] ; then ecmEnergy=13000 ; fi

# Find number of events to run from jO
jOPath=$(ls $ORIGDIR/$DSIDxxx/$DSID/mc.*.py)
nEventsPerJob=$(python $ORIGDIR/scripts/jo_utils.py --parameter="nEventsPerJob" --jO=$jOPath)

# Number of events to run = max(1,0.01*nEventsPerJob)
nEvents=$(bc <<< "if (1-0.01*$nEventsPerJob >0) 1 else 0.01*$nEventsPerJob/1")
  
# Check if it's an LHE-only jO
if [[ $(grep LHEonly $DSID/log.generate.short | awk '{print $NF}') == "True" ]] ; then
  LHEonly=true
  nEvents=1
else
  LHEonly=false
fi

# Check if it's a multicore process
cores=$(grep 'ATHENA_PROC_NUMBER =' $DSID/log.generate.short | awk '{print $NF}')
if [ ! -z $cores ] ; then
  # If the user asked for multi-core mode then use 4 cores and 4 events otherwise the jobs are very slow
  export ATHENA_PROC_NUMBER=4
  nEvents=4
  echo "INFO: will run in multi-core mode - cores originally requested: $cores - cores to be used: $ATHENA_PROC_NUMBER - events to generate: $nEvents"
else
  echo "INFO: will run in single-core mode"
fi

# Get the release to use from the log.generate.short file
rel=$(grep 'using release' $DSID/log.generate.short | awk '{print $NF}')
relName=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $1}')
relVer=$(echo $rel | awk 'BEGIN {FS="-"} ; {print $2}')
minorRel=$(echo $relVer | awk 'BEGIN {FS="."} ; {print $3}')

# Set up release
asetup $relVer,$relName

# Create a temporary directory from which we will run athena
mkdir tmp_$DSID
cd tmp_$DSID
TMP_DSID_DIR=$PWD

# If external LHE is used setup rucio and download file
inputGeneratorFile=$(grep inputGeneratorFile ../$DSID/log.generate.short)
if [[ ! -z $inputGeneratorFile ]] ; then
  # Set rucio account
  export RUCIO_ACCOUNT=cgutscho
  # Set up voms proxy
  echo "Job with external input files requested. Setting up voms proxy..."
  lsetup "rucio -w"
  echo $GRID_PW | voms-proxy-init --voms atlas --pwstdin --cert /root/.globus/usercert.pem --key /root/.globus/userkey.pem
  # Get the actual files to download
  echo $inputGeneratorFile | awk -F'inputGeneratorFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' '{printf "mc15_13TeV:%s\n", $NF}' > filesToDownload
  # Download the file
  rucio get `cat filesToDownload`
  mv mc15_13TeV/* . && rm -rf mc15_13TeV
  # Check if the file has been downloaded - if not exit with error
  for file in `cat filesToDownload | sed 's/mc15_13TeV://'` ; do
    if [ ! -f "$file" ] ; then
        echo "ERROR: file $file could not be downloaded from the grid"
        exit 1
    else
      echo "OK: file $file successfully downloaded"
    fi
  done
fi

# If there is a GRID file then copy it to the directory where we run athena
GRIDfile=$(ls ../$DSID/*.GRID.tar.gz 2>/dev/null)
if [ ! -z "$GRIDfile" ] ; then
  link=$GRIDfile
  # We need to iterate here to resolve relative links
  relLink=0
  while $(test -L $link) ; do
    link=$(readlink $link)
    # Jump to the directory where the parent link is located
    cd $(dirname $link) 2> /dev/null
    let relLink=$relLink+1
    if [ $relLink -gt 10 ] ; then # This should never happen, but you never know...
      echo "ERROR: file $GRIDfile is a link that points back to itself"
      exit 1
    fi
  done
  # Go back to DSID directory where we started
  cd $TMP_DSID_DIR
  
  echo "INFO: GRID file $GRIDfile will be copied from $link"
  GRIDfilename=$(basename $link)
  # Remove the link
  rm -f $GRIDfile
  # And copy the actual file
  if [[ "$link" == "/eos/user"* ]] ; then
    HOST=root://eosuser.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  elif [[ "$link" == "/eos/atlas"*  ]] ; then
    HOST=root://eosatlas.cern.ch
    xrdcp $HOST/$link ../$DSID/$GRIDfilename
  else
    cp $link ../$DSID/$GRIDfilename
  fi
  
  if [ -f ../$DSID/$GRIDfilename ] ; then
    echo "OK: File successfully copied"
  else
    echo "ERROR: Copying GRID file failed"
    exit 1
  fi
fi

# Set up generation command
com="Gen_tf.py --ecmEnergy=$ecmEnergy \
  --jobConfig=../$DSID \
  --maxEvents=$nEvents"
if $LHEonly; then
  com="${com} --outputTXTFile=LHE.TXT.tar.gz"
else
  com="${com} --outputEVNTFile=EVNT.root"
  if [[ ! -z $inputGeneratorFile ]] ; then
    com="${com} --inputGeneratorFile=$(echo $inputGeneratorFile | awk -F'inputGeneratorFile = ' '{print $NF}' | tr ',' '\n' | awk -F'/' '{printf "%s,", $NF} ' | sed 's/,*$//g')"
  fi
fi

# Run athena
echo "Will now run $com"
set -e # to catch any error from the execution of Gen_tf
eval $com
set +e # to avoid exiting from the script if any other setup command fails in the loop

# Update the number of jobs processed
let jobsProcessed=$jobsProcessed+1

# Rename log.generate output from transform to log.generate_ci
# which is uploaded as an artifact
if [ -f log.generate ] ; then
  mv log.generate ../$DSID/log.generate_ci
else
  echo "ERROR: no log.generate file produced"
  exit 1
fi

# Move to main directory
cd $ORIGDIR

exit 0

