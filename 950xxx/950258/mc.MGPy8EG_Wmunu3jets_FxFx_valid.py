import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Wmunu+3Np FxFx'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
evgenConfig.keywords+=['W','jets']
evgenConfig.generators += ["aMcAtNlo","Pythia8"]

# General settings
evgenConfig.nEventsPerJob = 50000
evgenConfig.inputFilesPerJob=1

# Shower/merging settings
parton_shower='PYTHIA8'
nJetMax=3
qCut=20.

#### Shower: Py8 with A14 Tune, with FxFx prescription
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py") 
include("Pythia8_i/Pythia8_aMcAtNlo.py")

# FxFx Matching settings, according to authors prescriptions (NB: it changes tune pars)
PYTHIA8_nJetMax=nJetMax
PYTHIA8_qCut=qCut
print "PYTHIA8_nJetMax = %i"%PYTHIA8_nJetMax
print "PYTHIA8_qCut = %i"%PYTHIA8_qCut

include("Pythia8_i/Pythia8_FxFx_A14mod.py")
