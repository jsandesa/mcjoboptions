#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with V1.0 of ATLAS/CMS Common settings for both Powheg and Pythia8.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar']
evgenConfig.contact     = [ 'm.fenton@cern.ch']
evgenConfig.generators  = [ 'Powheg', 'Pythia8' ]

evgenConfig.nEventsPerJob = 20000

include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.PDF = 260000  #NNPDF3.0
PowhegConfig.mu_F = 1  #facscfact
PowhegConfig.mu_R = 1  #renscfact

PowhegConfig.nEvents     *= 1.2

PowhegConfig.hdamp = 250.0

PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.__setattr__("mass_t", 172.5) 
PowhegConfig.__setattr__("width_t", 1.315) #tdec/twidth

PowhegConfig.__setattr__("mass_W", 80.4) #tdec/wmass
PowhegConfig.__setattr__("width_W", 2.11) #tdec/wwidth
PowhegConfig.__setattr__("BR_W_to_enu", 0.1081) #tdec/elbranching
PowhegConfig.__setattr__("sin2cabibbo", 0.051) #tdec/sin2cabibbo

PowhegConfig.__setattr__("mass_e", 0.00051) #tdec/emass
PowhegConfig.__setattr__("mass_mu", 0.1057) #tdec/mumass
PowhegConfig.__setattr__("mass_tau", 1.777) #tdec/taumass

PowhegConfig.__setattr__("mass_d", 0.21) #tdec/dmass
PowhegConfig.__setattr__("mass_u", 0.21) #tdec/umass
PowhegConfig.__setattr__("mass_s", 0.35) #tdec/smass
PowhegConfig.__setattr__("mass_c", 1.525) #tdec/cmass
PowhegConfig.__setattr__("mass_b", 4.875) #tdec/bmass

#PowhegConfig.__setattr__("use-old-grid", 1) 
#PowhegConfig.__setattr__("use-old-ubound", 1)
#PowhegConfig.__setattr__("ncall1", 10000)
#PowhegConfig.__setattr__("itmx1", 5)
#PowhegConfig.__setattr__("ncall2", 100000)
#PowhegConfig.__setattr__("itmx2", 5)
#PowhegConfig.__setattr__("foldcsi", 1)
#PowhegConfig.__setattr__("foldy", 1)
#PowhegConfig.__setattr__("foldphi", 1)
#PowhegConfig.__setattr__("nubound", 100000)
#PowhegConfig.__setattr__("iymax", 1)
#PowhegConfig.__setattr__("xupbound", 2)

PowhegConfig.generate()

include("Pythia8_i/Pythia8_Monash_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")


genSeq.Pythia8.Commands += ['POWHEG:veto = 1',
                            'POWHEG:pTdef = 1',
                            'POWHEG:emitted = 0',
                            'POWHEG:pTemt = 0',
                            'POWHEG:pThard = 0',
                            'POWHEG:vetoCount = 50',
                            'POWHEG:nFinal = 2',
                            'POWHEG:veto = 1',
                            'POWHEG:MPIveto = 0',
]

