
#### Shower
evgenConfig.description = 'aMcAtNlo_ttW_NLO'
evgenConfig.keywords+=['SM','ttW']
evgenConfig.contact = ['chris.g@cern.ch' ]
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 2

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Reset to serial processing
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_aMcAtNlo.py")
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
