include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")


evgenConfig.description = "ttll+0j@NLO+1,2,3j@LO with Sherpa-2.2.11"
evgenConfig.keywords = ["ttZ", "SM", "multilepton"]
evgenConfig.contact = ["rohin.narayan@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
    #perturbative scales
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;

    # merging setup
    QCUT:=30.;
    LJET:=4; NJET:=3;
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR 0.05
    NLO_CSS_PSMODE=1

    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[6] 0;

    SPECIAL_TAU_SPIN_CORRELATIONS=1
    SOFT_SPIN_CORRELATIONS=1
}(run)
(processes){
    Process 93 93 -> 6 -6 90 90 93{NJET};
    Order (*,2);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    End process;
}(processes)
(selector){
    Mass 11 -11  1 E_CMS;
    Mass 13 -13  1 E_CMS;
    Mass 15 -15  1 E_CMS;
}(selector)
"""
genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0","OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1"]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.Parameters += [ "OL_PREFIX=./Process/OpenLoops" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "pplltt", "ppllttj" ]
