#based on 361033
evgenConfig.description = "Inelastic minimum bias, with A14 NNPDF23LO tune and EvtGen"

evgenConfig.keywords = ["QCD", "minBias", "SM"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

evgenConfig.nEventsPerJob = 1000
