## JIRA ticket

ATLMCPROD-**please fill this**

## Checklist for requesters

- [ ] I have followed the [guidelines in McSampleRequestProcedure](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/McSampleRequestProcedure)
- [ ] I have provided the ATLMCPROD JIRA ticket where the request has been discussed
- [ ] The request has been approved by the PA/CP group

## Checklist for approvers

- [ ] All the items in the "Checklist for requesters" have been completed successfully
- [ ] The pipeline has run and the status is green (if it is orange look at the CI job output and iterate with the requesters to sort out the issues if possible)
- [ ] CI jobs have not been skipped (if jobs have been skipped, the requesters should provide a reason for skipping and confirmation from the PMG conveners is necessary before merging)
- [ ] Look at the output of the child pipeline created by `run_child_pipeline` and make sure that **at least one `run_athena_DSID` job has run**. If it's not the case, confirmation from the PMG conveners is necessary before merging
- [ ] Check that no `log.generate.short` files are included in the commit to be merged to master. If such files are present, it indicates that something went wrong in the pipeline. Check the job logs and contact the package maintainers if needed.
- [ ] Check that no file has been modified or deleted
- [ ] Check that the title of the MR is descriptive enough (if not ask the requester to modify it)
- [ ] Check that the DSID range assigned is correct (500000-999999) and corresponds to [right generator block](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgMcSoftware#DSID_blocks)

/label ~jobOptions
/assign @joany @cgutscho
