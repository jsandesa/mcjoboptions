# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()

gentype=JOName.split('SM')[1].split('_')[1]
if 'SLN1' in JOName: decaytype='twostepN2_SLN1'


mass_string = JOName.replace('.py','').split('N1_')[1]
deltaM = 0 # mchi20 - mchi10 

#--------------------------------------------------------------
# MadGrpah configuration
#--------------------------------------------------------------
if gentype=='GG':
# Direct gluino decay to LSP (0-lepton, grid 1 last year)
    masses['1000021'] = float( mass_string.split('_')[0] )  #gluino
    masses['1000022'] = float( mass_string.split('_')[1] )  #chi10
    masses['1000023'] = 0.5*(masses['1000021']+masses['1000022'])  #chi20
    deltaM = 0.5*(masses['1000021'] - masses['1000022']) # (mgluino - mchi10) / 2 = mchi20 - mchi10 

    process = '''
    generate p p > go go
    add process p p > go go j
    add process p p > go go j j
    '''
    decays["1000021"] = """DECAY   1000021     7.40992706E-02   # gluino decays
    #           BR         NDA      ID1       ID2       ID3
    2.00000000E-01    3     1000023         1        -1   # BR(~g -> ~chi_2^0 d  db)
    2.00000000E-01    3     1000023         2        -2   # BR(~g -> ~chi_2^0 u  ub)
    2.00000000E-01    3     1000023         3        -3   # BR(~g -> ~chi_2^0 s  sb)
    2.00000000E-01    3     1000023         4        -4   # BR(~g -> ~chi_2^0 c  cb)
    2.00000000E-01    3     1000023         5        -5   # BR(~g -> ~chi_2^0 b  bb)    """

elif gentype == "SS":
    masses['1000001'] = float( mass_string.split('_')[0] ) # squark mass
    masses['1000002'] = float( mass_string.split('_')[0] ) # squark mass
    masses['1000003'] = float( mass_string.split('_')[0] ) # squark mass
    masses['1000004'] = float( mass_string.split('_')[0] ) # squark mass

    masses['2000001'] = float( mass_string.split('_')[0] ) # squark mass
    masses['2000002'] = float( mass_string.split('_')[0] ) # squark mass
    masses['2000003'] = float( mass_string.split('_')[0] ) # squark mass
    masses['2000004'] = float( mass_string.split('_')[0] ) # squark mass

    masses['1000022'] = float( mass_string.split('_')[1] )  #chi10
    masses['1000023'] = 0.5*(masses['1000001']+masses['1000022'])  #chi20
    masses['1000024'] = masses['1000023'] #chi1+
    deltaM = 0.5*(masses['1000001'] - masses['1000022']) # (msquark - mchi10) / 2 = mchi20 - mchi10 

    process = ''' 
    generate p p > susylq susylq~ $ go susyweak @1
    add process p p > susylq susylq~ j $ go susyweak @2
    add process p p > susylq susylq~ j j $ go susyweak @3
    '''
    decays["1000001"]="""DECAY   1000001     1.31836627E+01   # sdown_L decays
    #          BR         NDA      ID1       ID2
    5.00000000E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
    5.00000000E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)"""
    
    decays["2000001"]="""DECAY   2000001     3.95203891E+00   # sdown_R decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2    -1000024         2   # BR(~d_R -> ~chi_1- u)
     5.00000000E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)"""
    
    decays["1000002"]="""DECAY   1000002     1.31301150E+01   # sup_L decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     5.00000000E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)"""
    
    decays["2000002"]="""DECAY   2000002     5.45626401E+00   # sup_R decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000024         1   # BR(~u_R -> ~chi_1+ d)
     5.00000000E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)"""
    
    decays["1000003"]="""DECAY   1000003     1.31836627E+01   # sstrange_L decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     5.00000000E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)"""
    
    decays["2000003"]="""DECAY   2000003     3.95203891E+00   # sstrange_R decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2    -1000024         4   # BR(~s_R -> ~chi_1- c)
     5.00000000E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)"""
    
    decays["1000004"]="""DECAY   1000004     1.31301150E+01   # scharm_L decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     5.00000000E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)"""
    
    decays["2000004"]="""DECAY   2000004     5.45626401E+00   # scharm_R decays
    #          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000024         3   # BR(~c_R -> ~chi_1+ s)
     5.00000000E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)"""
    
    decays["1000024"]="""DECAY   1000024     7.00367294E-03   # chargino1+ decays
    #          BR         NDA      ID1       ID2
     1.66666667E-01    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL  e+  )
     1.66666667E-01    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
     1.66666667E-01    2     1000016       -15   # BR(~chi_1+ -> ~nu_tau1 tau+)
     1.66666667E-01    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
     1.66666667E-01    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
     1.66666667E-01    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)"""

if 'SL' in decaytype:
    masses['1000011'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    masses['1000012'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    masses['1000013'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    masses['1000014'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    masses['1000015'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
    masses['1000016'] = 0.5*(masses['1000022']+masses['1000023'])  #slepton
else:
    masses['1000011'] = 4.5e5 #slepton
    masses['1000012'] = 4.5e5 #slepton
    masses['1000013'] = 4.5e5 #slepton
    masses['1000014'] = 4.5e5 #slepton
    masses['1000015'] = 4.5e5 #slepton
    masses['1000016'] = 4.5e5 #slepton

decays["1000023"] = """DECAY   1000023     9.37327589E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
8.33333333E-02    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
8.33333333E-02    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
8.33333333E-02    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
8.33333333E-02    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
8.33333333E-02    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
8.33333333E-02    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
8.33333333E-02    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
8.33333333E-02    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
8.33333333E-02    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
8.33333333E-02    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
8.33333333E-02    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
8.33333333E-02    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )    """

decays["1000011"] = """DECAY   1000011     1.00000000E-01   # selectron_L decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)    """

decays["1000013"] = """DECAY   1000013     1.00000000E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)    """

decays["1000015"] = """DECAY   1000015     1.00000000E-01   # stau_1 decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)    """

decays["1000012"] = """DECAY   1000012     1.00000000E-01   # snu_eL decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)    """

decays["1000014"] = """DECAY   1000014     1.00000000E-01   # snu_muL decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)    """

decays["1000016"] = """DECAY   1000016     1.00000000E-01   # snu_tauL decays
#          BR         NDA      ID1       ID2
1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)    """


evgenConfig.contact  = [ "liuya@cern.ch" ]
if 'GG' in gentype: 
    evgenConfig.keywords += ['simplifiedModel','gluino']

    if 'SL' in decaytype:
        evgenConfig.description = 'SUSY Simplified Model with gluino production and decays via sleptons with MadGraph/Pythia8, m_glu = %s GeV, m_N2 = %s GeV, m_slep = %s GeV, m_N1 = %s GeV'%(masses['1000021'],masses['1000023'],masses['1000011'],masses['1000022'])
        evgenConfig.keywords += ['slepton']

if 'SS' in gentype: 
    evgenConfig.keywords += ['simplifiedModel','squark']
 
    if 'SL' in decaytype:
        evgenConfig.description = 'SUSY Simplified Model with squark production and decays via sleptons with MadGraph/Pythia8, m_sq = %s GeV, m_N2 = %s GeV, m_slep = %s GeV, m_N1 = %s GeV'%(masses['1000001'],masses['1000023'],masses['1000011'],masses['1000022'])
        evgenConfig.keywords += ['slepton']
    
#--------------------------------------------------------------
# Pythia configuration
#--------------------------------------------------------------
# This comes after all Simplified Model setup files
evgenLog.info('Will use Pythia8...')

pythia = genSeq.Pythia8

pythia.MaxFailures = 100
#pythia.Commands += ["Check:nErrList = 10"]
#pythia.Commands += ["Check:particleData = on"]

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
filters=[]
# Two-lepton+MET filter
if '2LMET100' in JOName:
    evt_multiplier = 20
    include('GeneratorFilters/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2  
    
    include("GeneratorFilters/MissingEtFilter.py")
    filtSeq.MissingEtFilter.METCut = 100000.            # MET > 100 GeV
  
    filtSeq.Expression = "(MultiLeptonFilter) and (MissingEtFilter)"
    
    
# two lepton filter
elif '2L' in JOName:
    evt_multiplier = 20
    include('GeneratorFilters/MultiLeptonFilter.py')
    MultiLeptonFilter = filtSeq.MultiLeptonFilter
    filtSeq.MultiLeptonFilter.Ptcut = 5000.
    filtSeq.MultiLeptonFilter.Etacut = 2.8
    filtSeq.MultiLeptonFilter.NLeptons = 2  
    

# only MET filter
elif 'MET100' in JOName:
    evt_multiplier *= 2
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    MissingEtFilter = filtSeq.MissingEtFilter
    filtSeq.MissingEtFilter.METCut = 100000.

njets = 1
include( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

    
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = guess" ]
    
    ### needed to use "guess" option of Pythia.
    if "UserHooks" in genSeq.Pythia8.__slots__.keys():
        genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    else:
        genSeq.Pythia8.UserHook = 'JetMergingaMCatNLO'
