'''
R21.6 job options for photon fusion production of slepton pairs

For useful links with helpful information, see:
  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/SUSYMcRequestProcedureRel21
  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYMcRequestProcedure
  https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/blob/master/503xxx/503054/SUSY_SimplifiedModel_SlepSlep_direct_MET50.py
  https://gitlab.cern.ch/atlas/athena/-/blob/release/21.6.54/Generators/MadGraphControl/share/common/SUSY_SimplifiedModel_PreInclude.py
'''
from MadGraphControl.MadGraphUtils import *

phys_short = get_physics_short()
def StringToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

mslep      = StringToFloat(phys_short.split('_')[4]) 
mn1        = StringToFloat(phys_short.split('_')[5])

#-----------------------------------------
# Set masses in the parameter card 
#-----------------------------------------
# Following 
decoupled_mass = '4.5E9'
masses = {}
for p in ['1000001','1000002','1000003','1000004','1000005','1000006','2000001','2000002','2000003','2000004','2000005','2000006','1000021',\
          '1000023','1000024','1000025','1000011','1000013','1000015','2000011','2000013','2000015','1000012','1000014','1000016','1000022',\
          '1000035','1000037','35','36','37']: # Note that gravitino is non-standard
    masses[p]=decoupled_mass

masses['1000011'] = mslep # selectron_L
masses['2000011'] = mslep # selectron_R
masses['1000013'] = mslep # smuon_L
masses['2000013'] = mslep # smuon_1
masses['1000015'] = mslep # stau_1
masses['2000015'] = mslep # stau_2
masses['1000022'] = mn1   # N1_LSP

#-----------------------------------------
# Set decays in the parameter card 
#-----------------------------------------
decays = {}
# Specify slepton decays
for l in [11,13,15]:
    susy_p = str(1000000+l)
    decays[susy_p]="""DECAY   %s     1.00000000E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        %s   # BR(~mu_L -> ~chi_10 lepton)
"""%(susy_p,str(l))
    susy_p = str(2000000+l)
    decays[susy_p]="""DECAY   %s     1.00000000E-01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        %s   # BR(~mu_L -> ~chi_10 lepton)
"""%(susy_p,str(l))

# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures
nevents=5*runArgs.maxEvents if runArgs.maxEvents>0 else 5500
#mode=0

# Specify MadGraph process, decaying sleptons in MadGraph
process = '''
import MSSM_SLHA2
define lep = e+ mu+ ta+ e- mu- ta-
define slep = el- el+ er- er+ mul- mul+ mur- mur+ 
generate a a > slep slep, slep > lep n1
output -f
''' 

# Modify default run_card.dat settings
run_settings = {'event_norm'      :'sum',
                'drjj'            :0.0,
                'lhe_version'     :'3.0',
                'cut_decays'      :'F',
                'lpp1'            :'2', # 2 = photon initiated from proton
                'lpp2'            :'2',
                'dsqrt_q2fact1'   :2., 
                'dsqrt_q2fact2'   :2.,
                'fixed_fac_scale' :'T', 
                'pdgs_for_merging_cut': '1, 2, 3, 4, 21', # Terrible default in MG
                'ickkw'           :0,
                'xqcut'           :0,# use CKKW-L merging (yes, this is a weird setting)
                'use_syst'        :'F',
                'nevents'         :int(nevents)
                } 

# Metadata
evgenConfig.generators  = ["MadGraph"]
evgenConfig.contact     = [ "kristin.lohwasser@cern.ch", "jesse.liu@cern.ch" ]
evgenConfig.description = 'Photon fusion production of selectron/smuon pairs, direct decay to electrons/muons + neutralino dark matter'
evgenLog.info('a a > slep slep with m(slep, n1) = ({0}, {1}) GeV'.format(mslep, mn1))
evgenConfig.keywords   +=["SUSY", "Sleptons", "Photon fusion production"]
#runName='PROC_sm_0/run_01' 

#-------------------------------------------------------------- 
# Generate MadGraph events
#-------------------------------------------------------------- 
# Set up the process
process_dir = new_process(process)
# Set up the run card (run_card.dat)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_settings)
# Set up the parameter card (param_card.dat)
modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays})
# Generate the events
generate(process_dir=process_dir,runArgs=runArgs)
# Remember to set saveProcDir to FALSE before sending for production!!
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

#-------------------------------------------------------------- 
# Shower JOs will go here
#-------------------------------------------------------------- 
include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_EE_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")    
include("Pythia8_i//Pythia8_MadGraph.py")
