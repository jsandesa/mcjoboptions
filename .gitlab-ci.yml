#-----------------------------
# VARIABLES
#-----------------------------
variables:
  GIT_SSL_NO_VERIFY: "true"

#-----------------------------
# PIPELINE STAGES
#-----------------------------
stages:
  - check_commit
  - setup
  - run
  - cleanup
  - allow_merge

#-----------------------------
# BUILD: CHECK JO CONSISTENCY
#-----------------------------
check_jo_consistency:
  stage: check_commit
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  script:
    - python -B scripts/check_jo_consistency_main.py

#----------------------------------
# BUILD: CHECK UNIQUE CONTROL FILE
#----------------------------------
check_unique_file:
  stage: check_commit
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  script:
    - ./scripts/check_unique_controlFile.sh
  allow_failure: true
  
#----------------------------------
# BUILD: CHECK UNIQUE PHYSICS SHORT
#----------------------------------
check_unique_physicsShort:
  stage: check_commit
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  script:
    - ./scripts/check_unique_physicsShort.sh

#-----------------------------------------------------
# BUILD: CHECK OTHER FILES FOR MODIFICATIONS/DELETIONS
#-----------------------------------------------------
check_modified_files:
  stage: check_commit
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip modfiles\]/
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  before_script:
    - git config --global user.name "mcgensvc" && git config --global user.email "mcgensvc@cern.ch"
  script:
    - ./scripts/check_modified_files.sh

#---------------------------------
# BUILD: CHECK TYPE OF ADDED FILES
#---------------------------------
check_added_files:
  stage: check_commit
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  script:
    - ./scripts/check_added_files.sh

#--------------------------------------------
# BUILD: CHECK GRID FILE READABILITY & NFILES
#--------------------------------------------
check_tarball:
  stage: check_commit
  tags:
    - cvmfs
  # Have to use private image built from gitlab-registry.cern.ch/dss/eos:4.4.47
  # since official image doesn't have correct entrypoint
  image: gitlab-registry.cern.ch/atlas-physics/pmg/mcjoboptions:eos_bash
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  before_script:
    - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
  script:
    - ./scripts/check_tarball.sh

#-----------------------------------------------------
# BUILD: CHECK GRID FILE SIZE
#-----------------------------------------------------
check_grid_size:
  stage: check_commit
  tags:
    - cvmfs
  # Have to use private image built from gitlab-registry.cern.ch/dss/eos:4.4.47
  # since official image doesn't have correct entrypoint
  image: gitlab-registry.cern.ch/atlas-physics/pmg/mcjoboptions:eos_bash
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  before_script:
      - echo ${K8S_SECRET_SERVICE_PASSWORD} | kinit ${SERVICE_ACCOUNT}@CERN.CH
  script:
    - ./scripts/check_grid_size.sh
  allow_failure: true

#-----------------------------------------------------
# SETUP ATHENA: GENERATE ATHENA JOBS
#-----------------------------------------------------
setup_athena:
  stage: setup
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip athena\]/
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  artifacts:
    paths:
      - .run_athena.yml
  script:
    - python scripts/generateYaml.py 5 # generate 5 athena jobs
    - echo "Automatically generated CI config file:"
    - cat .run_athena.yml

#-----------------------------------------------------
# RUN ATHENA: RUN ATHENA PARENT JOB
#-----------------------------------------------------
run_child_pipeline:
  stage: run
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip athena\]/
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  trigger:
    include:
      - artifact: .run_athena.yml
        job: setup_athena
    strategy: depend

#-----------------------------------------------------
# CLEAN UP: REMOVE LOG.GENERATE.SHORT FILES
#-----------------------------------------------------
remove_logs:
  stage: cleanup
  except:
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  before_script:
    - git config --global user.name "mcgensvc" && git config --global user.email "mcgensvc@cern.ch"
    - git checkout -b ${CI_COMMIT_REF_NAME} || echo "Branch ${CI_COMMIT_REF_NAME} already exists, checking it out" && git checkout ${CI_COMMIT_REF_NAME}
  script:
    - ./scripts/removeLogs.sh


#-------------------------------------------------------------------
# ALLOW MERGE: DUMMY JOB THAT RUNS ONLY IF EVERYTHING ELSE SUCCEEDED
#-------------------------------------------------------------------
allow_merge:
  stage: allow_merge
  only:
    refs:
      - merge_requests
    variables:
      - $CI_COMMIT_MESSAGE =~ /\[skip all\]/
  script:
    - echo "Pipeline successful"
