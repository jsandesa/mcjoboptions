include ('MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphParamHelpers import get_masses, get_block

process = '''
generate p p > n1 n2  $ susystrong @1
add process p p > n1 x1+  $ susystrong @1
add process p p > n1 x1-  $ susystrong @1
add process p p > n2 x1+  $ susystrong @1
add process p p > n2 x1-  $ susystrong @1
add process p p > x1+ x1-  $ susystrong @1

add process p p > n1 n2 j $ susystrong @2
add process p p > n1 x1+ j $ susystrong @2
add process p p > n1 x1- j $ susystrong @2
add process p p > n2 x1+ j $ susystrong @2
add process p p > n2 x1- j $ susystrong @2
add process p p > x1+ x1- j $ susystrong @2

add process p p > n1 n2 j j $ susystrong @3
add process p p > n1 x1+ j j $ susystrong @3
add process p p > n1 x1- j j $ susystrong @3
add process p p > n2 x1+ j j $ susystrong @3
add process p p > n2 x1- j j $ susystrong @3
add process p p > x1+ x1- j j $ susystrong @3
'''

njets=2

### parse job options name 
# N.B. 60 chars limit on PhysicsShort name...
phys_short = get_physics_short()
evgenLog.info('Physics short name: %s' % phys_short)

# format: mc.MGPy8EG_A14NNPDF23LO_GGM_N1N2C1_XXXX 
mN1 = phys_short.split('_')[4]


# Masses
masses['1000022'] = float(mN1) # N1 mass
masses['1000023'] = -(float(mN1) + 11)
masses['1000024'] = float(mN1) + 10
masses['1000039'] = 1E-9

# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'

# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'

# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 5.00E-01'   # N_11
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 6.12E-01'   # N_13
param_blocks['NMIX']['1  4']='-6.12E-01'   # N_14
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24
param_blocks['NMIX']['3  1']=' 8.66E-01'   # N_31
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32
param_blocks['NMIX']['3  3']='-3.53E-01'   # N_33
param_blocks['NMIX']['3  4']=' 3.53E-01'   # N_34
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44

# Decays
decays['1000022'] = '''DECAY   1000022     1.0E-05   # neutralino1 decays
#          BR         NDA      ID1       ID2
    3.3333E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
    3.3333E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
    3.3333E-01    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
'''

decays['1000023']='''DECAY   1000023     1.0E-05   # neutralino2 decays
	1.2000E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
	1.6000E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
	1.2000E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
	1.6000E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
	1.2000E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
	3.5500E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
	3.5500E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
	3.5500E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
	7.0000E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
	7.0000E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
	7.0000E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
#
'''

decays['1000024']='''DECAY   1000024     1.0E-05   # chargino1+ decays
	3.3333E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
	3.3333E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
	1.1111E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
	1.1111E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
	1.1111E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
'''




evgenLog.info('Registered generation of simplified model direct higgsino/bino to gravitino grid')
evgenConfig.contact = ['orellana.g@cern.ch', 'hernan.wahlberg@cern.ch', 'francisco.alonso@cern.ch']
evgenConfig.keywords += ['SUSY', 'chargino', 'neutralino', 'simplifiedModel']
evgenConfig.description = 'GGM Chargino/Neutralinos production, bino-higgsino mix, simplified model: mN1 = %s GeV' % mN1

include ('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')

if njets>0:
	genSeq.Pythia8.Commands += ['Merging:Process = guess']
	genSeq.Pythia8.Commands += ['1000039:m0 = %f' % masses['1000039']] # Add gravitino mass for this model from original param card 
	genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']

