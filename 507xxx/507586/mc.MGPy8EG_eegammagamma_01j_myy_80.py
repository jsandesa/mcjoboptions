import os
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

#evgenConfig.inputconfcheck=""

nevents = runArgs.maxEvents*2.2 if runArgs.maxEvents>0 else 2.2*evgenConfig.nEventsPerJob
gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    set stdout_level DEBUG
    import model loop_sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define e = e+ e-
    generate p p > e e a a [QCD] @0
    add process p p > e e a a j [QCD] @1
    output -f"""

    process_dir = str(new_process(process))
else: 
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

#settings = { 'pdlabel':"'nn23nlo'",
settings = { 'parton_shower':'PYTHIA8',
             'nevents':int(nevents),
             'maxjetflavor':5,
             'ickkw':3,
             'ptj':8.0,
             'jetalgo':1.0,
             'jetradius':1.0,
             'ptgmin':17.0,
             'R0gamma':0.1,
             'xn':2.0,
             'epsgamma':0.1,
             'isoEM':'True'}

os.system("cp pty_17_myy_80_cuts.f "+process_dir+"/SubProcesses/cuts.f")
os.system("sed -i '/132/ a FFLAGS+= -mcmodel=medium' "+process_dir+"/Source/make_opts")

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

evgenConfig.generators = ["aMcAtNlo"]

check_reset_proc_number(opts)

############################
# Shower JOs will go here

evgenConfig.description = 'aMcAtNlo_enugammagamma'
evgenConfig.keywords+=['Z','2photon','jets','NLO']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Alessandro Ambler <aambler@cern.ch>']
evgenConfig.nEventsPerJob = 5000

PYTHIA8_nJetMax=1
PYTHIA8_qCut=20.0
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_FxFx_A14mod.py")
