from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------
evgenConfig.description   = 'allhadronic ttbar with EFT LO model dim6top, 2LQ2HQ coefficients'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
#evgenConfig.keywords     += ['ttbar','dim6top']
evgenConfig.keywords     += ['ttbar']
evgenConfig.contact       = ['roman.lysak@cern.ch']
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------
jo_name = get_physics_short()

process='''
    set stdout_level DEBUG
    import model dim6top_LO_UFO
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > t t~ QED=0 QCD=2 FCNC=0, (t > w+ b FCNC=0,  w+ > j j FCNC=0 DIM6=0), (t~ > w- b~ FCNC=0, w- > j j FCNC=0 DIM6=0) @0 DIM6^2=2
    output -f
'''

process_dir = new_process(process)

# Fix f2py
modify_config_card(process_dir=process_dir,settings={'f2py_compiler':'f2py2','f2py_compiler_py2':'f2py2'})

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents *= 1.1 # safety factor

settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : '5',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'pdlabel'               : '"lhapdf"',
    'lhaid'                 : '260000',
    'use_syst'              : 'True',
    'systematics_program'   : 'systematics',
    'systematics_arguments' : ['--mur=0.5,1.0,2.0','--muf=0.5,1.0,2.0','--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i','--dyn=3','--pdf=errorset,13100@0,25200@0,265000@0,266000@0'],
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '3',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['5'] = '0.0'
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['yukawa']['5'] = '0.0'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
params['sminputs'] = dict()
params['sminputs']['1'] = '1.323489e+02' # change aewm1 to restore correct W mass

modify_param_card(process_dir=process_dir,params=params)

# Reset dim6top parameters
params = dict() 
params['dim6'] = dict()
params['dim6']['1'] = '1.000000e+03'
params['dim6']['51'] = '0.2'  #start reweighting from non-SM sample 
# Write the updated param card
modify_param_card(process_dir=process_dir,params=params)

# --------------------------------------------------------------
#  reweight_card
# --------------------------------------------------------------
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write('''
launch --rwgt_info=reset_do_not_use
'''+process_dir+'/Cards/param_card.dat'+'''
launch --rwgt_info=SM
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_p0p2
set DIM6 51  0.2
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_p0p5
set DIM6 51  0.5
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_m0p4
set DIM6 51  -0.4
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_m0p7
set DIM6 51  -0.7
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_p0p2
set DIM6 51  0.0
set DIM6 52  0.2
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_p0p4
set DIM6 51  0.0
set DIM6 52  0.4
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_m0p3
set DIM6 51  0.0
set DIM6 52  -0.3
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_m0p5
set DIM6 51  0.0
set DIM6 52  -0.5
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_p0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.3
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_p0p6
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.6
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_m0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  -0.3
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_m0p6
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  -0.6
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.4
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_p0p7
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.7
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  -0.4
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_m0p7
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  -0.7
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID55_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID55_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.4
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID55_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  -0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID55_m0p6
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  -0.6
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_p0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.3
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_p0p5
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.5
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_m0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  -0.3
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_m0p7
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  -0.7
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID57_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID57_p0p7
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.7
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID57_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  -0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID57_m0p9
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  -0.9
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_p0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.12
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.4
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_m0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  -0.12
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  -0.4
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_p0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.12
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.4
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_m0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  -0.12
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  -0.4
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID60_p0p15
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.15
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID60_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.4
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID60_m0p15
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  -0.15
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID60_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  -0.4
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID61_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.2
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID61_p0p5
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.5
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID61_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  -0.2
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID61_m0p5
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  -0.5
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID62_p0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.12
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID62_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.4
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID62_m0p12
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  -0.12
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID62_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  -0.4
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID63_p0p16
set DIM6 51  0.0
set DIM6 52  0.0 
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.16
set DIM6 64  0.0
launch --rwgt_info=ID63_p0p4
set DIM6 51  0.0
set DIM6 52  0.0 
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.4
set DIM6 64  0.0
launch --rwgt_info=ID63_m0p16
set DIM6 51  0.0
set DIM6 52  0.0 
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  -0.16
set DIM6 64  0.0
launch --rwgt_info=ID63_m0p4
set DIM6 51  0.0
set DIM6 52  0.0 
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  -0.4
set DIM6 64  0.0
launch --rwgt_info=ID64_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.2
launch --rwgt_info=ID64_p0p5
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.5
launch --rwgt_info=ID64_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  -0.2
launch --rwgt_info=ID64_m0p5
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  -0.5
launch --rwgt_info=ID51_ID52_p0p2_p0p2
set DIM6 51  0.2
set DIM6 52  0.2
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_ID52_m0p2_m0p2
set DIM6 51  -0.2
set DIM6 52  -0.2
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_ID55_p0p2_p0p2
set DIM6 51  0.2
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID51_ID55_m0p2_m0p2
set DIM6 51  -0.2
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  -0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_ID55_p0p2_p0p2
set DIM6 51  0.0
set DIM6 52  0.2
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID52_ID55_m0p2_m0p2
set DIM6 51  0.0
set DIM6 52  -0.2
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  -0.2
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_ID56_p0p3_p0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.3
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.3
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID53_ID56_m0p3_m0p3
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  -0.3
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  -0.3
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_ID57_p0p4_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.4
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID54_ID57_m0p4_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  -0.4
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  -0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_ID57_p0p3_p0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.3
set DIM6 57  0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID56_ID57_m0p3_m0p4
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  -0.3
set DIM6 57  -0.4
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_ID59_p0p2_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.2
set DIM6 59  0.2
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_ID59_m0p2_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  -0.2
set DIM6 59  -0.2
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_ID62_p0p2_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.2
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.2
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID58_ID62_m0p2_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  -0.2
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  -0.2
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_ID62_p0p2_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.2
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  0.2
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID59_ID62_m0p2_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  -0.2
set DIM6 60  0.0
set DIM6 61  0.0
set DIM6 62  -0.2
set DIM6 63  0.0
set DIM6 64  0.0
launch --rwgt_info=ID60_ID63_p0p4_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.4
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  0.2
set DIM6 64  0.0
launch --rwgt_info=ID60_ID63_m0p4_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  -0.4
set DIM6 61  0.0
set DIM6 62  0.0
set DIM6 63  -0.2
set DIM6 64  0.0
launch --rwgt_info=ID61_ID64_p0p2_p0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  0.2
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  0.2
launch --rwgt_info=ID61_ID64_m0p2_m0p2
set DIM6 51  0.0
set DIM6 52  0.0
set DIM6 53  0.0
set DIM6 54  0.0
set DIM6 55  0.0
set DIM6 56  0.0
set DIM6 57  0.0
set DIM6 58  0.0
set DIM6 59  0.0
set DIM6 60  0.0
set DIM6 61  -0.2
set DIM6 62  0.0
set DIM6 63  0.0
set DIM6 64  -0.2
''')
reweight_card_f.close()

print_cards()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
