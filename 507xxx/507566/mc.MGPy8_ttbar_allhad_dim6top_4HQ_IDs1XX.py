from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------
evgenConfig.description   = 'allhadronic ttbar with EFT LO model dim6top, 4HQ coefficients'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
#evgenConfig.keywords     += ['ttbar','dim6top']
evgenConfig.keywords     += ['ttbar']
evgenConfig.contact       = ['roman.lysak@cern.ch']
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------
jo_name = get_physics_short()

process='''
    set stdout_level DEBUG
    import model dim6top_LO_UFO
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > t t~ QED=0 QCD=2 FCNC=0, (t > w+ b FCNC=0,  w+ > j j FCNC=0 DIM6=0), (t~ > w- b~ FCNC=0, w- > j j FCNC=0 DIM6=0) @0 DIM6^2=2
    output -f
'''

process_dir = new_process(process)

# Fix f2py
modify_config_card(process_dir=process_dir,settings={'f2py_compiler':'f2py2','f2py_compiler_py2':'f2py2'})

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents *= 1.1 # safety factor

settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : '5',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'pdlabel'               : '"lhapdf"',
    'lhaid'                 : '260000',
    'use_syst'              : 'True',
    'systematics_program'   : 'systematics',
    'systematics_arguments' : ['--mur=0.5,1.0,2.0','--muf=0.5,1.0,2.0','--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i','--dyn=3','--pdf=errorset,13100@0,25200@0,265000@0,266000@0'],
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '3',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['5'] = '0.0'
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['yukawa']['5'] = '0.0'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
params['sminputs'] = dict()
params['sminputs']['1'] = '1.323489e+02' # change aewm1 to restore correct W mass

modify_param_card(process_dir=process_dir,params=params)

# Reset dim6top parameters
params = dict() 
params['dim6'] = dict()
params['dim6']['1'] = '1.000000e+03'
params['dim6']['94'] = '2.000000e+00'
# Write the updated param card
modify_param_card(process_dir=process_dir,params=params)

# --------------------------------------------------------------
#  reweight_card
# --------------------------------------------------------------
reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write('''
launch --rwgt_info=reset_do_not_use
'''+process_dir+'/Cards/param_card.dat'+'''
launch --rwgt_info=SM
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID94_p2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  2.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID95_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  5.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID96_p2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  2.0
set DIM6 97  0.0
launch --rwgt_info=ID97_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  5.0
launch --rwgt_info=ID94_m2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  -2.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID95_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  -5.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID96_m2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  -2.0
set DIM6 97  0.0
launch --rwgt_info=ID97_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  -5.0
launch --rwgt_info=ID94_p4p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  4.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID95_p8p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  8.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID96_p4p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  4.0
set DIM6 97  0.0
launch --rwgt_info=ID97_p8p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  8.0
launch --rwgt_info=ID94_m4p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  -4.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID95_m8p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  -8.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID96_m4p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  -4.0
set DIM6 97  0.0
launch --rwgt_info=ID97_m8p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  -8.0
launch --rwgt_info=ID94_ID95_p2p0_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  2.0
set DIM6 95  5.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID94_ID95_m2p0_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  -2.0
set DIM6 95  -5.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID96_ID97_p2p0_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  2.0
set DIM6 97  5.0
launch --rwgt_info=ID96_ID97_m2p0_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  -2.0
set DIM6 97  -5.0
launch --rwgt_info=ID94_ID96_p2p0_p2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  2.0
set DIM6 95  0.0
set DIM6 96  2.0
set DIM6 97  0.0
launch --rwgt_info=ID94_ID97_p2p0_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  2.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  5.0
launch --rwgt_info=ID95_ID96_p5p0_p2p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  5.0
set DIM6 96  2.0
set DIM6 97  0.0
launch --rwgt_info=ID95_ID97_p5p0_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  5.0
set DIM6 96  0.0
set DIM6 97  5.0
launch --rwgt_info=ID65_p3p0
set DIM6  65  3.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_p6p0
set DIM6  65  0.0
set DIM6  66  6.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID67_p3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  3.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_p3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  3.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID70_p3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  3.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID65_m3p0
set DIM6  65  -3.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_m6p0
set DIM6  65  0.0
set DIM6  66  -6.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID67_m3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  -3.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_m3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  -3.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID70_m3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  -3.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID65_p5p0
set DIM6  65  5.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_p10p0
set DIM6  65  0.0
set DIM6  66  10.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID67_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  5.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  5.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID70_p5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  5.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID65_m5p0
set DIM6  65  -5.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_m10p0
set DIM6  65  0.0
set DIM6  66  -10.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID67_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  -5.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  -5.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID70_m5p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  -5.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID65_ID67_p3p0_p3p0
set DIM6  65  3.0
set DIM6  66  0.0
set DIM6  67  3.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID65_ID67_m3p0_m3p0
set DIM6  65  -3.0
set DIM6  66  0.0
set DIM6  67  -3.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_ID71_p6p0_p6p0
set DIM6  65  0.0
set DIM6  66  6.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  6.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID66_ID71_m6p0_m6p0
set DIM6  65  0.0
set DIM6  66  -6.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  -6.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_ID70_p3p0_p3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  3.0
set DIM6  69  0.0
set DIM6  70  3.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID68_ID70_m3p0_m3p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  -3.0
set DIM6  69  0.0
set DIM6  70  -3.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID71_p6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  6.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_p6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  6.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID73_p6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  6.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID71_m6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  -6.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_m6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  -6.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID73_m6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  -6.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID71_p10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  10.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_p10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  10.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID73_p10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  10.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID71_m10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  -10.0
set DIM6  72  0.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_m10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  -10.0
set DIM6  73  0.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID73_m10p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  0.0
set DIM6  73  -10.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_ID73_p6p0_p6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  6.0
set DIM6  73  6.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
launch --rwgt_info=ID72_ID73_m6p0_m6p0
set DIM6  65  0.0
set DIM6  66  0.0
set DIM6  67  0.0
set DIM6  68  0.0
set DIM6  69  0.0
set DIM6  70  0.0
set DIM6  71  0.0
set DIM6  72  -6.0
set DIM6  73  -6.0
set DIM6 94  0.0
set DIM6 95  0.0
set DIM6 96  0.0
set DIM6 97  0.0
''')
reweight_card_f.close()

print_cards()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
