model="LightVector"
fs = "ee"
mDM1 = 500.
mDM2 = 2000.
mZp = 1000.
mHD = 125.
filteff = 9.961152e-01

evgenConfig.description = "Mono Z' sample - model Light Vector"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MGPy8EG_mono_zp_lep.py")
