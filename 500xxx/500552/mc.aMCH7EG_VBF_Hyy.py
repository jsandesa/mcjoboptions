
from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


evgenConfig.keywords    = ['VBF', 'Higgs', 'mH125', 'gluonFusionHiggs', 'resonance']
evgenConfig.contact     = ['ana.cueto@cern.ch']
evgenConfig.generators  = ['aMcAtNlo', 'Herwig7', 'EvtGen']
evgenConfig.description = "MG+H7.1 VBF at NLO"
evgenConfig.tune = "H7.1-Default"

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

#Used for shell commands.
import subprocess

######################################################################
# Configure event generation.
######################################################################

gridpack_mode=False
if not is_gen_from_gridpack():
    process = """
    import model loop_qcd_qed_sm_Gmu 
    define p = g u c b d s u~ c~ d~ s~ b~                                                                      
    define j = g u c b d s u~ c~ d~ s~ b~                                                                      
    generate p p > h j j $$ w+ w- z / a [QCD]  
    output -f
    """
    
    process_dir = new_process(process)
else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION



#Fetch default run_card.dat and set parameters
settings= {'ptj'           : 10,
          'jetradius'     : 1.0,
          'maxjetflavor'  : 5,
          'parton_shower' :'PYTHIA8',
          'mll_sf'        : 10.0,
          'muR_ref_fixed' : 125.0,
          'muF1_ref_fixed': 125.0,
          'muF2_ref_fixed': 125.0,
          'QES_ref_fixed' : 125.0,
          'nevents'      :int(nevents)
          }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)


masses={'25': '1.250000e+02'}
params={}
params['MASS']=masses
modify_param_card(process_dir=process_dir,params=params)

# fix check_poles issue
replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")


generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
#arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True,outputDS=runName+'._00001.events.tar.gz')
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)




print "Now performing parton showering ..."
   
######################################################################
# End of event generation, start configuring parton shower here.
######################################################################
'''
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
else: opts.nprocs = 0
    print opts
'''

#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
#Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")
include("Herwig7_i/Herwig71_EvtGen.py")
# only consider H->gammagamma decays
Herwig7Config.add_commands("""
  # force H->yy decays                                                                                                      
  do /Herwig/Particles/h0:SelectDecayModes h0->gamma,gamma;                                                                 
  # print out decays modes and branching ratios to the terminal/log.generate                                                
  do /Herwig/Particles/h0:PrintDecayModes
""")
# run Herwig7
Herwig7Config.run()

