##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B+ -> J/psi(mu3.5mu3.5) K+
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B+\n")
f.write("1.0000  myJ/psi   K+             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

## Initialise Herwig7 for run with built-in matrix elements
include("Herwig7_i/Herwig7_BuiltinME.py")
include("Herwig7_i/Herwig71_EvtGen.py")

## Provide config information
evgenConfig.description = "Exclusive Bplus->Jpsi_mu3p5mu3p5_Kplus production"
evgenConfig.keywords = ["exclusive","Bplus","Jpsi","2muon"]
evgenConfig.generators += ["Herwig7"]
evgenConfig.tune        = "H7.2-Default"
evgenConfig.contact     = ["Javier Llorente (javier.llorente.merino@cern.ch)"]

evgenConfig.nEventsPerJob = 1000

## Configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="MMHT2014lo68cl")

command = """
insert /Herwig/MatrixElements/SubProcess:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
"""

print command

Herwig7Config.add_commands(command)

## run the generator
Herwig7Config.run()

##Add EvtGen
genSeq.EvtInclusiveDecay.outputKeyName = "GEN_EVENT"
genSeq.EvtInclusiveDecay.readExisting = True
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"
genSeq.EvtInclusiveDecay.whiteList+=[521]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
evgenConfig.auxfiles += [ 'BU_JPSI_K_USER.DEC' ]

##Filter for Bp
include("GeneratorFilters/BSignalFilter.py")
filtSeq.BSignalFilter.LVL1MuonCutOn = True
filtSeq.BSignalFilter.LVL2MuonCutOn = True
filtSeq.BSignalFilter.LVL1MuonCutPT = 5500
filtSeq.BSignalFilter.LVL1MuonCutEta = 2.6
filtSeq.BSignalFilter.LVL2MuonCutPT = 5500
filtSeq.BSignalFilter.LVL2MuonCutEta = 2.6

filtSeq.BSignalFilter.B_PDGCode = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

#Jet filter
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)
include("GeneratorFilters/JetFilter_JZX.py")
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 15*GeV
