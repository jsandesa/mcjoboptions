#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["Pythia8"]
evgenConfig.description = "Pythia8 dijet production with A14 NNPDF2.3 tune, filtered in phi and eta."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["jahreda@gmail.com"]
evgenConfig.nEventsPerJob = 1000

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
###include("Pythia8_i/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection = on",
                            "PhaseSpace:pTHatMin = 150."]

####genSeq.Pythia8.Commands += ["SoftQCD:inelastic = on"]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
filtSeq.QCDTruthJetFilter.MinEta = 0.0
filtSeq.QCDTruthJetFilter.MaxEta = 3.5
filtSeq.QCDTruthJetFilter.MinPhi = 0.3
filtSeq.QCDTruthJetFilter.MaxPhi = 0.5
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(4, filtSeq)

