evgenConfig.description = "Pythia8B J/Psi J/Psi -> 4 muon in double parton scattering"
evgenConfig.process = "J/Psi J/Psi -> 4mu"
evgenConfig.keywords = ["4Muon","SM","heavyFlavour"]
evgenConfig.generators = ["Pythia8B"]
evgenConfig.contact  = ["Xin.Chen@cern.ch"]
evgenConfig.nEventsPerJob = 200

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include("Pythia8B_i/Pythia8B_Photospp.py")
include("Pythia8B_i/Pythia8B_Charmonium_Common.py")

###  shower weights ##############

#################################
genSeq.Pythia8B.Commands += [
        "Charmonium:states(3S1) = 443,100443",
        "Charmonium:gg2ccbar(3S1)[3S1(1)]g = on,on",
        "Charmonium:gg2ccbar(3S1)[3S1(8)]g = on,on",
        "Charmonium:qg2ccbar(3S1)[3S1(8)]q = on,on",
        "Charmonium:qqbar2ccbar(3S1)[3S1(8)]g = on,on",
        "Charmonium:gg2ccbar(3S1)[1S0(8)]g = on,on",
        "Charmonium:qg2ccbar(3S1)[1S0(8)]q = on,on",
        "Charmonium:qqbar2ccbar(3S1)[1S0(8)]g = on,on",
        "Charmonium:gg2ccbar(3S1)[3PJ(8)]g = on,on",
        "Charmonium:qg2ccbar(3S1)[3PJ(8)]q = on,on",
        "Charmonium:qqbar2ccbar(3S1)[3PJ(8)]g = on,on",
        "Charmonium:O(3S1)[3S1(1)] = 1.16,0.76",
        "Charmonium:O(3S1)[3S1(8)] = 0.0119,0.0050",
        "Charmonium:O(3S1)[1S0(8)] = 0.01,0.004",
        "Charmonium:O(3S1)[3P0(8)] = 0.01,0.004",
        "Charmonium:states(3PJ) = 10441,20443,445",
        "Charmonium:O(3PJ)[3P0(1)] = 0.00,0.00,0.00",
        "Charmonium:O(3PJ)[3S1(8)] = 0.0000,0.0000,0.0000",
        "Charmonium:states(3DJ) = 30443",
        "Charmonium:O(3DJ)[3D1(1)] = 0.000",
        "Charmonium:O(3DJ)[3P0(8)] = 0.00",
        "SecondHard:generate = on",
        "SecondHard:Charmonium = on",
        "Init:showChangedSettings = on",
        "Next:numberShowProcess = 2",
        "PartonLevel:MPI = on",
        "PartonLevel:ISR = on",
        "PartonLevel:FSR = on",
        "HadronLevel:Hadronize = on",
        "HadronLevel:Decay = on"]

genSeq.Pythia8B.Commands += [
        "100443:onMode = off",
        "100443:onIfMatch = 11 11", # ee
        "100443:onIfMatch = 13 13", # mumu
        "100443:onIfMatch = 15 15", # tautau
        "100443:onIfAny = 443", # Jpsi
        "100443:onIfAny = 445", # chi_2c
        "100443:onIfAny = 10441", # chi_0c
        "100443:onIfAny = 10443", # h_1c
        "100443:onIfAny = 20443", # chi_1c
        "100443:0:bRatio = 0.01658", # ee
        "100443:1:bRatio = 0.1231", # mumu
        "100443:2:bRatio = 0.04722", # tautau
        "100443:21:bRatio = 0.0013", # Jpsi+pi0
        "100443:22:bRatio = 0.0324", # Jpsi+eta
        "100443:43:bRatio = 0.1658", # Jpsi+2pi0
        "100443:44:bRatio = 0.3366", # Jpsi+2pi
        "100443:23:bRatio = 0.0933", # chi_2c+gamma
        "100443:35:bRatio = 0.0922", # chi_0c+gamma
        "100443:36:bRatio = 0.0008", # h_1c+pi0
        "100443:37:bRatio = 0.0907", # chi_1c+gamma
        "443:onMode = off", #
        "443:onIfMatch = 13 13"]  # Close all J/psi decays apart from J/psi->mumu

genSeq.Pythia8B.SignalPDGCodes = [443,-13,13]

genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5, 2.5, 1.5] #GeV
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [2, 3, 4]

### Set lepton filters
if not hasattr(filtSeq, "M4MuIntervalFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import M4MuIntervalFilter
   M4MuIntervalfilter = M4MuIntervalFilter("M4MuIntervalfilter")
   filtSeq += M4MuIntervalfilter

filtSeq.M4MuIntervalfilter.MinPt = 1500 #MeV
filtSeq.M4MuIntervalfilter.MaxEta = 2.7
filtSeq.M4MuIntervalfilter.LowM4muProbability = 1.0
filtSeq.M4MuIntervalfilter.MediumMj4muProbability = 0.3
filtSeq.M4MuIntervalfilter.HighM4muProbability = 0.1
filtSeq.M4MuIntervalfilter.LowM4mu = 11000.
filtSeq.M4MuIntervalfilter.HighM4mu = 25000.
filtSeq.M4MuIntervalfilter.ApplyReWeighting = True


