evgenConfig.description = "Single pi0 with log energy distribution"
evgenConfig.keywords = ["singleParticle", "pi0"]
evgenConfig.contact = ["angerami@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (111)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(200, 2000000.), eta=[-3.0, 3.0])

include("EvtGen_i/EvtGen_Fragment.py")
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles+=['inclusive.pdt']
genSeq.EvtInclusiveDecay.allowAllKnownDecays=True
