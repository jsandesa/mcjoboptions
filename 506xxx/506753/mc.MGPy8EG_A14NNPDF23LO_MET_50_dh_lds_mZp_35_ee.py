model="darkHiggs" 
fs = "ee" 
mDM1 = 5. 
mDM2 = 5. 
mZp = 35. 
mHD = 125. 
filteff = 4.672897e-01 

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")