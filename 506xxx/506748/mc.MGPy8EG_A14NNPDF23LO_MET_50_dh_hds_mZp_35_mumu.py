model="darkHiggs" 
fs = "mumu" 
mDM1 = 5. 
mDM2 = 5. 
mZp = 35. 
mHD = 35. 
filteff = 1.305483e-01 

evgenConfig.description = "Mono Z' sample - model Dark Higgs"
evgenConfig.keywords = ["exotic","BSM","WIMP"]
evgenConfig.contact = ["Even S. Haaland <even.simonsen.haaland@cern.ch>"]

include("MadGraphControl_MGPy8EG_mono_zp_lep.py")