from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# General settings
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


# MadGraph Config
process = """
import model Radion_EW_all_gauge_Rgg_decay
define l = l+ l-
define v = vl vl~
define wkk = wkk+ wkk-
generate p p > wkk, (wkk > r l v, r > g g)
output -f
"""

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
                settings={
                'nevents':runArgs.maxEvents*1.1,
                'lhe_version':'3.0'
                }
                )

print_cards()

params = { 'kkinputs' : { 'MKK' : MKK },
               'mass' : { 'MR' : MR },
              'DECAY' : { '9000022' : str(WBKK),
                          '9000023' : str(WW3KK),
                          '9000024' : str(WWKK),
                          '9000025' : str(WR)
                        }
         }

modify_param_card(process_dir=process_dir, params=params)

pdgid_file = open('pdg_extras.dat','w')
pdgid_file.write("""
9000022
9000023
-9000023
9000024
-9000024
9000025
""")
pdgid_file.close()
testSeq.TestHepMC.UnknownPDGIDFile='pdg_extras.dat'

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3)

#### Shower 

evgenConfig.description = 'p p > wkk+, (wkk+ > r w+, r > g g) - model Radion_EW_all_gauge_inv'  
# keywords must from list: https://svnweb.cern.ch/cern/wsvn/atlasoff/Generators/MC15JobOptions/trunk/common/evgenkeywords.txt
evgenConfig.keywords+=["exotic","BSM","WIMP"]
evgenConfig.process = 'p p > wkk+, (wkk+ > r w+, r > g g)'
evgenConfig.contact = ['huan.yu.meng@cern.ch']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")        
include("Pythia8_i/Pythia8_MadGraph.py")       


#########
