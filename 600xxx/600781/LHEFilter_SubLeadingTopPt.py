
from AthenaCommon import Logging
athMsgLog = Logging.logging.getLogger('MCJobOptionUtils/LHEFilter.py')

# This code is stored in topcontrol 
include('MCJobOptionUtils/LHEFilter.py')
# This code is stored in gencontrol
include('MCJobOptionUtils/LHETools.py')
# This code is stored in gencontrol
#include("MCJobOptionUtils/merge_lhe_files.py")

class LHEFilter_SubLeadingTopPt(BaseLHEFilter):
    def __init__(self):
        # name of the filter, usefull for logfiles
        self.name = "SubLeadingTopPt"
        # set to True to invert the filter
        self.inverted = False
        # parameters for this filter
        self.Ptcut = 0. # no pT cut by default

    def pass_filter(self,Evt):
        # first count the leptons
        leadTopPt = 0.
        subleadTopPt = 0.
        for p in Evt:
            if abs(p.pdg_id) !=6:
                continue
            pT = (p.px**2 + p.py**2)**0.5
            if pT>leadTopPt:
                subleadTopPt = leadTopPt
                leadTopPt = pT
            elif pT<=leadTopPt:
                subleadTopPt = pT
            #elif pT>0. and pT>subleadTopPt:
            #    subleadTopPt = pT
        # then decide if we keep the event
        if subleadTopPt>=self.Ptcut:
            #athMsgLog.info(" Leading and subleading top pT are {} {}".format(float(leadTopPt), float(subleadTopPt)))
            return True
        else:
            return False
