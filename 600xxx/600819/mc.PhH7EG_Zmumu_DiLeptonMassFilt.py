#---------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Herwig7 Z->mumu production without lepton filter.'
evgenConfig.contact = ["Christian Gutschow <chris.g@cern.ch>"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2muon' ]
evgenConfig.generators  = [ 'Powheg', 'Herwig7', 'EvtGen' ]
evgenConfig.tune        = "H7.1-Default"
evgenConfig.nEventsPerJob = 1000
# Dilepton pT cut of >100 GeV has about 1.2% efficiency -> 100x more LHE events
filterMultiplier = 100
#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults 
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > mu+ mu-"

# Configure Powheg setup
PowhegConfig.nEvents = runArgs.maxEvents*filterMultiplier if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMultiplier
#PowhegConfig.nEvents *= 1.2 # increase number of generated events by 20%
PowhegConfig.running_width = 1

PowhegConfig.generate()

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering of LHE files
include("Herwig7_i/Herwig7_LHEF.py")
# configure Herwig7
Herwig7Config.me_pdf_commands(order="NLO", name="CT10nlo")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")
# run Herwig7
Herwig7Config.run()

# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    
include("GeneratorFilters/DiLeptonMassFilter.py")
DiLeptonMassFilter = filtSeq.DiLeptonMassFilter
DiLeptonMassFilter.MinPt = 5000.
DiLeptonMassFilter.MaxEta = 3.0
DiLeptonMassFilter.MinMass = 10000.
DiLeptonMassFilter.MaxMass = 1E9
DiLeptonMassFilter.MinDilepPt = 100000.
DiLeptonMassFilter.AllowSameCharge = False
