from MadGraphControl.MadGraphUtils import *

include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

evt_multiplier = 25.0 # Needed due to pythia filters
joparts = runArgs.jobConfig[0].rstrip('.py').split('/')

masses['1000021'] = mg 
masses['1000022'] = mN 
if masses['1000022']<0.5: masses['1000022']=0.5
gentype = genType 
decaytype = decayType 
short_decaytype = shortDecayType 
neutralino_ctau = Nctau 

# Calculate neutralino width:
neutralino_width = 3.*1e11*6.581e-25/neutralino_ctau

decays = {'1000022':"""DECAY   1000022  """   +str(neutralino_width)+  """ # neutralino decay
##            BR         NDA      ID1       ID2
        1.00000000E+00    2     1000039        23   #  BR(~chi_10 -> ~G Z)""",
          '1000021':"""DECAY   1000021     7.40992706E-02   # gluino decays
##            BR         NDA      ID1       ID2
        2.50000000E-01    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
        2.50000000E-01    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
        2.50000000E-01    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
        2.50000000E-01    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)"""}

# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']='1.00000000E+00'   # N_11 bino 
param_blocks['NMIX']['1  2']='0.00000000E+00'   # N_12
param_blocks['NMIX']['1  3']='0.00000000E+00'   # N_13
param_blocks['NMIX']['1  4']='0.00000000E+00'   # N_14 
param_blocks['NMIX']['2  1']='0.00000000E+00'   # N_21 
param_blocks['NMIX']['2  2']='1.00000000E+00'   # N_22
param_blocks['NMIX']['2  3']='0.00000000E+00'   # N_23 higgsino
param_blocks['NMIX']['2  4']='0.00000000E+00'   # N_24 higgsino 
param_blocks['NMIX']['3  1']='0.00000000E+00'   # N_31 
param_blocks['NMIX']['3  2']='0.00000000E+00'   # N_32 
param_blocks['NMIX']['3  3']='1.00000000E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']='0.00000000E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']='0.00000000E+00'   # N_41
param_blocks['NMIX']['4  2']='0.00000000E+00'   # N_42 wino
param_blocks['NMIX']['4  3']='0.00000000E+00'   # N_43
param_blocks['NMIX']['4  4']='1.00000000E+00'   # N_44

process = '''
import model MSSM_SLHA2

# Define multiparticle labels
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define pb = g u c d s b u~ c~ d~ s~ b~
define jb = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~

# Specify process(es) to run
generate p p > go go $ susysq susysq~ @0
add process p p > go go j $ susysq susysq~ @1
add process p p > go go j j $ susysq susysq~ @2

# Output processes to MadEvent directory
output -f
'''

njets = 2
evgenLog.info('Registered generation of gluino grid '+str(joparts[len(joparts)-1]))

process_dir = new_process(process)
params={}
params['MASS']=masses
params['DECAY']=decays

settings = {'small_width_treatment'  :'1e-12'}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

evgenConfig.contact  = [ "victoria.sanchez.sebastian@cern.ch" ]
evgenConfig.keywords += ['simplifiedModel','gluino']
evgenConfig.description = 'gluino production in simplified model, GMSB scenario with displaced neutralino decay to ZG, m_glu = %s GeV, m_N1 = %s GeV, ctau(N1) = %s mm'%(masses['1000021'],masses['1000022'],neutralino_ctau)

randomSeed_original = 4321
if hasattr(runArgs, "randomSeed"):
    randomSeed_original = runArgs.randomSeed
# Giving a unique seed number (not exceeding the limit of ~30081^2)
runArgs.randomSeed = randomSeed_original 

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
runArgs.randomSeed = randomSeed_original

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]

#Change gravitino mass:
genSeq.Pythia8.Commands += ["1000039:m0 = 2.27500000E-07"] # Add gravitino mass for this model from original param card 
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay

# Turn off checks for displaced vertices. 
# Other checks are fine.
testSeq.TestHepMC.MaxVtxDisp = 1000*1000 #In mm
testSeq.TestHepMC.MaxTransVtxDisp = 1000*1000
